﻿using UnityEngine;

/// <summary>
/// This script is used to make objects follow another object when the 
/// other object is not a child of the object that you want to follow.  
/// </summary>
public class CameraController : MonoBehaviour
{

    public GameObject player;

    private Vector3 offset;
    private SteamVR_TrackedObject trackedObject;
    
    private SteamVR_Controller.Device controller
    {
        get { return SteamVR_Controller.Input((int)trackedObject.index);  }
    }

    void Awake()
    {
        trackedObject = gameObject.transform.Find("Controller (right)").GetComponent<SteamVR_TrackedObject>();
    }

    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    void LateUpdate()
    {
        transform.position = player.transform.position + offset;

        if (controller.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            Debug.Log("Trigger pressed..."); 
        }
    }
}