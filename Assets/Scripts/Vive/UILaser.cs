﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UILaser : MonoBehaviour {

    public GameObject LaserPrefab;
    public LayerMask UIMask;

    private SteamVR_TrackedObject trackedObj;
    private GameObject laser;
    private Transform laserTransform;
    private Vector3 hitPoint;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    private void ShowLaser(RaycastHit hit)
    {
        
        laser.SetActive(true);
        
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f);
        
        laserTransform.LookAt(hitPoint);
        
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance);
    }

    // Use this for initialization
    void Start()
    {
        
        laser = Instantiate(LaserPrefab);
        
        laserTransform = laser.transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
        {
            RaycastHit hit;
            
            if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, UIMask))
            {
                hitPoint = hit.point;
                ShowLaser(hit);
                Debug.Log("Found");
            } else
            {
            Debug.Log(UIMask.value);
            Debug.Log("Not Found UILaser");
            }
        }
        else 
        {
            laser.SetActive(false);
        }
    }

}
