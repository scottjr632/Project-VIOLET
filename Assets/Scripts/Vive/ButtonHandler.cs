﻿using UnityEngine;
using Helpers;

/// <summary>
/// Turns on the button parent so the the ToActive button is on and the ToInactive 
/// buttons are inactive when the left controller touches the parent.
/// </summary>
public class ButtonHandler : MonoBehaviour {

    [Tooltip("The objects that you want to be active.")]
    public GameObject ToActive;
    [Tooltip("All objects that you want to be inactive.")]
    public GameObject[] ToInactive;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "LeftController")
        { 
            Helper.DisableGameObjects(ToActive, ToInactive);
        }
    }
}
