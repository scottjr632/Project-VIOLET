﻿using UnityEngine;
using Helpers;

/// <summary>
/// This script is used to be able to change the buttons that are shown 
/// on top of the controller.
/// </summary>
public class ChangeButtons : MonoBehaviour {

    [Tooltip("Objects that you want to be active.")]
    public GameObject ToActive;
    [Tooltip("All objects that you want to be incative.")]
    public GameObject[] ToInactive;
    [Tooltip("All objects of the IR objects.")]
    public GameObject[] NonIRObjects;


    void OnTriggerEnter(Collider other)
    {
        // If the left controller hits the buttons.
        // Do not allow other objects to hit the buttons.
        if (other.tag == "LeftController")
        {   
            // If the ToActive object is alrady active.
            if (ToActive.activeInHierarchy)
            {
                // Set ToAcive object to inactive.
                ToActive.SetActive(false);
                // Set all IR objects to active.
                foreach (GameObject i in NonIRObjects)
                
                    i.SetActive(true);
                }
            } else
            { 
                // Turn on the ToActive object.
                Helper.DisableGameObjects(ToActive, ToInactive);
            }
        }
    }
