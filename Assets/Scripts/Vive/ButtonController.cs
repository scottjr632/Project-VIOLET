﻿using UnityEngine;

/// <summary>
/// This script brings up the menu for choosing between objects.
/// </summary>
public class ButtonController : MonoBehaviour {

    [Tooltip("This is the button parent.")]
    public GameObject ButtonHandler;

    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        ButtonHandler.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
        if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            if (!ButtonHandler.activeInHierarchy)
            {
                ButtonHandler.SetActive(true);
            } else
            {
                ButtonHandler.SetActive(false);
            }
        } else if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
            gameObject.transform.position = new Vector3(0, 0, 0);
        }
	}
}
