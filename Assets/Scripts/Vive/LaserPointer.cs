﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointer : MonoBehaviour {

    public GameObject LaserPrefab;
    public Transform CameraRigTransform;
    public GameObject TeleportReticlePrefab;
    public Transform HeadTransform;
    public Vector3 TeleportReticleOffset;
    public LayerMask TeleportMask;

    private GameObject reticle;
    private Transform teleportReticleTransform;
    private GameObject laser;
    private Transform laserTransform;
    private Vector3 hitPoint;
    private SteamVR_TrackedObject trackedObj;
    private bool shouldTeleport;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    private void ShowLaser(RaycastHit hit)
    {
        
        laser.SetActive(true);
        
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f);
        
        laserTransform.LookAt(hitPoint);
        
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance);
    }

    private void Teleport()
    {
        shouldTeleport = false;
        reticle.SetActive(false);
        Vector3 difference = CameraRigTransform.position - HeadTransform.position;
        difference.y = 0;
        CameraRigTransform.position = hitPoint + difference;
    }

    // Use this for initialization
    void Start () {
        
        laser = Instantiate(LaserPrefab);
        
        laserTransform = laser.transform;
        
        reticle = Instantiate(TeleportReticlePrefab);
        
        teleportReticleTransform = reticle.transform;
    }
	
	// Update is called once per frame
	void Update () {
        
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
        {
            RaycastHit hit;

            
            if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, TeleportMask))
            {
                hitPoint = hit.point;
                ShowLaser(hit);
                
                reticle.SetActive(true);
                
                teleportReticleTransform.position = hitPoint + TeleportReticleOffset;
                
                shouldTeleport = true;
            }
        }
        else 
        {
            laser.SetActive(false);
            reticle.SetActive(false);
        }
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Trigger))
        {
            laser.SetActive(false);
            shouldTeleport = false;
        }

        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) && shouldTeleport)
        {
            Teleport();
        }
    }
}
