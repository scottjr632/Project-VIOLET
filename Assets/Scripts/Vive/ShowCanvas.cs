﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Helpers;

public class ShowCanvas : MonoBehaviour {

    public GameObject Canvas;

    private bool canvasActive = true;
    private SteamVR_TrackedObject trackedObj;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }


    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

	
	// Update is called once per frame
	void Update () {
		
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
        {
            if (canvasActive)
            {
                Canvas.SetActive(false);
                canvasActive = false;
            } else
            {
                Canvas.SetActive(true);
                canvasActive = true;
            }
        }
	}
}
