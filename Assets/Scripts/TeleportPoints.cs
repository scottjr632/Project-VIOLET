﻿using UnityEngine;
using Valve.VR.InteractionSystem;

/// <summary>
/// This script is used to teleport the player to the defined the location on 
/// the actual teleportation point. 
/// </summary>
public class TeleportPoints : MonoBehaviour {

    private bool onPlane = false;
    private Transform plane;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<TeleportPoint>() != null)
        {
            try
            {
                gameObject.transform.position = other.GetComponent<TeleportPoint>().PointToTeleport.transform.position;
                // If the other transport location is on the plane then allow it to update the position so that the 
                // camera follows the plane.
                if (other.GetComponent<TeleportPoint>().PointToTeleport.tag == "Plane")
                {
                    plane =  other.GetComponent<TeleportPoint>().PointToTeleport.transform;
                    onPlane = true;
                }
                else
                {
                    onPlane = false;
                }
            }
            catch (UnassignedReferenceException) { /* This error happens when you transport to the plane and can be ignored */ }
        }
    }

    private void LateUpdate()
    {
        // If the camera is on the plane teleport than update the camera position so that it follows the 
        // trasnport point.
        if (onPlane)
        {
            gameObject.transform.position = plane.position;
        }
    }
}
