﻿using UnityEngine;

/// <summary>
/// Behavior, attributes, and logic for the pulse prefab. All of these
/// attributes are used to describe the behavior of the prefab.
/// </summary>
public class Pulse : MonoBehaviour {

    public float Speed = -2;    // Speed is used to set the direction of the pulse
    public int Bounces = 0;     // Bounces is used to destroy the pulse after N number of bounces
    public float Size = .9f;    // This is the initial size of the prefab
    public Color HitColor;
    // YAngle determines how much the Y changes throughout time.
    // This is actually set in the RadarPulse script.
    public float yAngle;

    private float growth = .4f;

    void OnTriggerEnter(Collider other)
    {
        // If the pulse runs into something other than another pulse
        if (other.tag != "Pulse")
        {
            // Add to the bounces. Max number of bounces is set at instantiate.
            Bounces++;
            try
            {
                // After the pulse hits a plane it sets the size of
                // pulse to the size of the RCS attribute from the attribure script.
                var attr = other.GetComponent<Attributes>();
                Size = attr.RCS;
            }
            catch
            {
                // If there is not an RCS arribute on the object reset the size.
                Size = .9f;
            }
            // Change the rate of growth so it does not grow after it hits something.
            growth = 0;
            // If it does hit something change the direction.
            Speed = -Speed;
            yAngle = -yAngle;
            // Change the color when the pulse hits something
            gameObject.GetComponent<Renderer>().material.color = HitColor;

        }
    }

    void LateUpdate()
    {
        // Grow the scale of pulse on each update.
        Size += growth;
    }
}
