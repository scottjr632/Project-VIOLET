﻿using System;
using UnityEngine;

namespace Helpers
{
    /// <summary>
    /// Helper class so allow for better scaling
    /// </summary>
    public class Helper
    {
        /// <summary>
        /// Set object to active and inactive
        /// </summary>
        /// <param name="toActive">GameObject to be set to active</param>
        /// <param name="toInActive">GameObject to be set to inactice</param>
        public static void DisableGameObjects(GameObject toActive, GameObject toInActive)
        {
            toActive.SetActive(true);
            toInActive.SetActive(false);
        }

        public static void DisableGameObjects(GameObject toActive, GameObject[] toInActive)
        {
            toActive.SetActive(true);
            foreach(var i in toInActive)
            {
                i.SetActive(false);
            }
        }

    }
        
}
