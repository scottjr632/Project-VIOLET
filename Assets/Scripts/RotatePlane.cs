﻿using UnityEngine;
using Valve.VR.InteractionSystem;

public class RotatePlane : MonoBehaviour {

    [Tooltip("The object you want the plane to rotate.")]
    public GameObject ObjectToRotate;
    public int RotateSpeed = 40;

    private bool rotate = true;
    private bool playerOn = false;

	void Update () {
        if (rotate)
        {
            // Rotates the attached game object around the given game object.
            gameObject.transform.RotateAround(ObjectToRotate.transform.position, Vector3.up, RotateSpeed * Time.deltaTime);
        }
        
	}

    private void OnTriggerEnter(Collider other)
    {
        // If the camera is attached to the plane then
        // set stop the plane from rotating.
        if (other.GetComponent<Player>() != null)
        {
            rotate = false;
            playerOn = true;
        } else
        {
            rotate = true;
            playerOn = false;
        }
    }
}
