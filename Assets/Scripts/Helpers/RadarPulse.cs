﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class RadarPulse : MonoBehaviour {

    public GameObject PulsePrefab;
    public Transform Transform;
    public int PulseDelay;
    public enum AxisToMove { x, y, z };
    public enum RADARMode { ew, ta }
    public AxisToMove AxisSelected;
    public RADARMode RadarMode;
    public int SpeedAndDirection;
    public int NumberOfPulses;
    public float YAngle;
    public Vector3 Rotation;

    private Vector3 startLocation;

    // This sets the update speed in the Lerp.
    private const float speed = .9f;
    // This dictionary is used to create multiple pulses within the same area. A Hash
    // Map is best for this so that you can look up the pulse by its PropertyName which 
    // allows it to run O(1).
    private Dictionary<PropertyName, PulseHelper> pulses = new Dictionary<PropertyName, PulseHelper>();

    /// <summary>
    /// Delays the pulse for the amount of seconds statet in the public field PulseDelay and 
    /// then sets a new pulse object as pulse.
    /// </summary>
    /// <returns></returns>
    private IEnumerator DelayPulse()
    {
        yield return new WaitForSeconds(PulseDelay);
        CreatePulse();
    }

    /// <summary>
    /// Delays the pulse for the amount of seconds stated in the argument and then creates
    /// sets a new pulse object as pulse.
    /// </summary>
    /// <param name="delayInSeconds"> Number of seconds to delay the pulse. </param>
    /// <returns></returns>
    private IEnumerator DelayPulse(int delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        CreatePulse();
    }

    /// <summary>
    /// Creates a new pulse from the provided prefab and adds it into the pulses dictionary.
    /// It also sets the pulse to the initial scale, and rotates the pulse to the required rotation.
    /// </summary>
    private void CreatePulse()
    {
        var newPulse = new PulseHelper(Instantiate(PulsePrefab));
        newPulse.Pulse.SetActive(true);
        newPulse.Transform.position = startLocation;
        newPulse.Transform.Rotate(Rotation);
        newPulse.Speed = SpeedAndDirection;
        newPulse.YAngle = (YAngle / Math.Abs(SpeedAndDirection));
        pulses.Add(newPulse.GetHashCode(), newPulse);
    }
    
	void Start () {
        // Create the first pulse and get all the positions.
        startLocation = Transform.position;
        startLocation.y += 20;              // This is offset because of the heigth of the RADAR and its collider
        startLocation.x += -2;              // This is offset because of the length of the RADAR and its collider
        // Create the number of pulses that was specified. Each pulse is created a second later 
        // so that they do not run into each other at the very start. This also seperates the pulses out.
        for (int i = 0; i < NumberOfPulses; i++)
        {
            StartCoroutine(DelayPulse(i));
        }
    }

    void Update()
    {
        // Create a list of the values in the dictionary. We need to use a list because 
        // iterating through a dictionary in the update function causes an out of sync error.
        var listOfPulses = pulses.Values.ToList();
        foreach (PulseHelper pulse in listOfPulses)
        {
            try
            {
                // Get the new position of the pulse
                Vector3 newPulsePosition = pulse.Transform.position;
                // Set the direction that the pulse should travel on the x,y,z axis
                switch (AxisSelected)
                {
                    case AxisToMove.x:
                        newPulsePosition.x += pulse.Speed;
                        newPulsePosition.y += pulse.YAngle;
                        break;
                    case AxisToMove.y:
                        newPulsePosition.y += pulse.Speed;
                        break;
                    case AxisToMove.z:
                        newPulsePosition.z += pulse.Speed;
                        newPulsePosition.y += pulse.YAngle;
                        break;
                    default:
                        break;
                }
                pulse.Transform.localScale = new Vector3(pulse.Transform.localScale.x,
                    pulse.Size, pulse.Transform.localScale.z);
                // Move the pulse
                pulse.Transform.position = Vector3.Lerp(pulse.Transform.position, newPulsePosition, speed);

                // Only allow the pulse to survive two reflections
                // Set the max size to 100 so the pulse does not live forever.
                if (pulse.Bounces >= 2 || 
                    pulse.Size > 100)
                {
                    pulses.Remove(pulse.GetHashCode());
                    Destroy(pulse.Pulse);
                    // Delays the pulse
                    StartCoroutine(DelayPulse());
                }
            }
            catch (MissingReferenceException) {/* This exception is caused by delaying the pulse so it is ignored */}
        }
    }

    /// <summary>
    /// Helper class to create an object for each pulse that is stored in a dictionary.
    /// This allows for multiple pulses to be created from the same object.
    /// </summary>
    private class PulseHelper
    {
        public GameObject Pulse;
        public PulseHelper(GameObject pulse)
        {
            Pulse = pulse;
        }
        /*
         * All of the get and setters below get the values from the script "Pulse" that is 
         * attached to pulse prefab. The scripts contains attributes for the pulse.
         */
        public Transform Transform { get { return Pulse.transform; } }
        public int Bounces { get { return Pulse.GetComponent<Pulse>().Bounces; } }

        public float Size
        {
            get { return Pulse.GetComponent<Pulse>().Size; }
            set { Pulse.GetComponent<Pulse>().Size = value; }
        }

        public float Speed
        {
            get { return Pulse.GetComponent<Pulse>().Speed; }
            set { Pulse.GetComponent<Pulse>().Speed = value; }
        }

        public float YAngle
        {
            get { return Pulse.GetComponent<Pulse>().yAngle; }
            set { Pulse.GetComponent<Pulse>().yAngle = value; }
        }

    }
}


