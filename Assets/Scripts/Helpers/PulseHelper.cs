﻿using UnityEngine;

namespace Helpers
{
    /// <summary>
    /// This class is used to help the RadarPulse scipt but is currently 
    /// not used. The RadarPulse script now uses a private child class.
    /// </summary>
    [System.Obsolete]
    public class PulseHelper : MonoBehaviour
    {
        public GameObject Pulse;
        public PulseHelper(GameObject pulse)
        {
            Pulse = pulse;
        }
        public Transform Transform { get { return Pulse.transform; } }

        public float Size
        {
            get { return Pulse.GetComponent<Pulse>().Size; }
            set { Pulse.GetComponent<Pulse>().Size = value; }
        }

        public float Speed
        {
            get { return Pulse.GetComponent<Pulse>().Speed; }
            set { Pulse.GetComponent<Pulse>().Speed = value; }
        }

        public int Bounces
        {
            get { return Pulse.GetComponent<Pulse>().Bounces; }
        }

    }
}