﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateController : MonoBehaviour {

    public Animator anim;
    public GameObject self;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	void OnTriggerEnter (Collider other) {
        if(other.tag == "LeftController")
        {
            anim.Play("animate_x35_ir_button");
        }
	}

    void Update()
    {
        if (Input.GetKeyDown("1"))
        {
            anim.Play("animate_x35_ir_button");
        }

        if (self.activeInHierarchy)
        {
            anim.Play("animate_x35_ir_button");
        }
    }
}
