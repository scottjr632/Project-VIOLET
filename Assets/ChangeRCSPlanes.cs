﻿using UnityEngine;

public class ChangeRCSPlanes : MonoBehaviour {

    public GameObject PlaneOne;
    public GameObject PlaneTwo;

    private SteamVR_TrackedObject trackedObj;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Update is called once per frame
    void Update()
    {

        // TODO: FIX THIS
        if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            if (!PlaneOne.activeInHierarchy)
            {
                PlaneTwo.SetActive(false);
                PlaneOne.SetActive(true);
            }
            else
            {
                PlaneOne.SetActive(false);
                PlaneTwo.SetActive(true);
            }
        }
    }
}
